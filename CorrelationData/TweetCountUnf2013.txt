Adams County	247	17	213	17	0
Allegheny County	19926	950	17388	936	652
Armstrong County	73	5	63	5	0
Beaver County	820	36	752	30	2
Bedford County	78	4	73	1	0
Berks County	1412	72	1247	50	43
Blair County	278	17	239	22	0
Bradford County	93	8	74	10	1
Bucks County	1695	90	1455	137	13
Butler County	538	33	482	23	0
Cambria County	434	29	364	25	16
Cameron County	5	2	3	0	0
Carbon County	145	11	128	6	0
Centre County	1260	89	1073	86	12
Chester County	2020	85	1801	128	6
Clarion County	100	4	86	9	1
Clearfield County	92	10	75	4	3
Clinton County	99	2	93	4	0
Columbia County	266	18	220	27	1
Crawford County	127	5	116	6	0
Cumberland County	783	40	700	39	4
Dauphin County	1963	77	1773	77	36
Delaware County	2111	121	1879	95	16
Elk County	24	3	21	0	0
Erie County	1578	94	1391	82	11
Fayette County	554	49	479	25	1
Forest County	2	0	2	0	0
Franklin County	397	24	347	22	4
Fulton County	14	1	13	0	0
Greene County	54	3	45	6	0
Huntingdon County	61	5	50	5	1
Indiana County	398	21	352	24	1
Jefferson County	73	3	68	2	0
Juniata County	19	2	16	1	0
Lackawanna County	1018	47	851	103	17
Lancaster County	1875	92	1697	75	11
Lawrence County	413	17	389	7	0
Lebanon County	179	8	152	19	0
Lehigh County	1532	47	1349	72	64
Luzerne County	761	34	672	24	31
Lycoming County	249	14	226	9	0
McKean County	79	3	70	6	0
Mercer County	532	29	479	19	5
Mifflin County	41	0	37	4	0
Monroe County	424	21	379	22	2
Montgomery County	2372	116	2067	160	29
Montour County	38	5	30	2	1
Northampton County	1506	62	1306	64	74
Northumberland County	82	3	70	9	0
Perry County	72	6	63	3	0
Philadelphia County	36584	1552	33387	1259	386
Pike County	65	3	58	3	1
Potter County	9	0	7	2	0
Schuylkill County	202	13	181	7	1
Snyder County	73	5	63	4	1
Somerset County	144	4	130	10	0
Sullivan County	2	0	2	0	0
Susquehanna County	18	0	17	1	0
Tioga County	58	4	51	3	0
Union County	63	4	53	6	0
Venango County	978	7	357	2	612
Warren County	38	1	35	2	0
Washington County	729	47	649	31	2
Wayne County	99	7	87	4	1
Westmoreland County	908	55	808	45	0
Wyoming County	35	2	30	3	0
York County	1557	89	1363	95	10