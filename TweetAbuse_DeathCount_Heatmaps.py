'''
***************************************************************************************************
This script accompanies the following paper:

Sarker A, Gonzalez-Hernandez G, Ruan Y, Perrone J.
Machine Learning and Natural Language Processing for Geolocation-Centric Monitoring and
Characterization of Opioid-Related Social Media Chatter.
JAMA Netw Open. 2019 Nov 1;2(11):e1914672. doi: 10.1001/jamanetworkopen.2019.14672.

The script was modified from several online examples. Please follow the parameter values specified
in the paper to obtain comparable results.

For questions, please contact:
Abeed Sarker (abeed@dbmi.emory.edu)
Yucheng Ruan (ycruan@seas.upenn.edu)

'''

import numpy as np
import pandas as pd

# library for geographic plotting
import plotly.figure_factory as ff


flag = 0 # 0 for death count and 1 for tweet abuse

# read tweet count
tweet_count_2012 = pd.read_csv('./CorrelationData/TweetCountUnf2012.txt', header=None, delimiter='\t', names=['county', 'count', 'label_1', 'label_2', 'label_3', 'label_4'])
tweet_count_2013 = pd.read_csv('./CorrelationData/TweetCountUnf2013.txt', header=None, delimiter='\t', names=['county', 'count', 'label_1', 'label_2', 'label_3', 'label_4'])
tweet_count_2014 = pd.read_csv('./CorrelationData/TweetCountUnf2014.txt', header=None, delimiter='\t', names=['county', 'count', 'label_1', 'label_2', 'label_3', 'label_4'])
tweet_count_2015 = pd.read_csv('./CorrelationData/TweetCountUnf2015.txt', header=None, delimiter='\t', names=['county', 'count', 'label_1', 'label_2', 'label_3', 'label_4'])

# read death data
death_raw_2012 = pd.read_csv('./CorrelationData/DeathRate2012.txt', header=None, delimiter='\t')[[0, 1, 6, 7]].rename(columns={0: 'county_state', 1: 'fips', 6: 'death', 7: 'population'})
death_raw_2013 = pd.read_csv('./CorrelationData/DeathRate2013.txt', header=None, delimiter='\t')[[0, 1, 6, 7]].rename(columns={0: 'county_state', 1: 'fips', 6: 'death', 7: 'population'})
death_raw_2014 = pd.read_csv('./CorrelationData/DeathRate2014.txt', header=None, delimiter='\t')[[0, 1, 6, 7]].rename(columns={0: 'county_state', 1: 'fips', 6: 'death', 7: 'population'})
death_raw_2015 = pd.read_csv('./CorrelationData/DeathRate2015.txt', header=None, delimiter='\t')[[0, 1, 6, 7]].rename(columns={0: 'county_state', 1: 'fips', 6: 'death', 7: 'population'})

# split the 'county_state' column into two columns and abstract the county
death_raw_2012['county'] = death_raw_2012['county_state'].str.split(',', expand=True)[0]
death_raw_2013['county'] = death_raw_2013['county_state'].str.split(',', expand=True)[0]
death_raw_2014['county'] = death_raw_2014['county_state'].str.split(',', expand=True)[0]
death_raw_2015['county'] = death_raw_2015['county_state'].str.split(',', expand=True)[0]

# merge the rows with same county
death_all_2012 = death_raw_2012.groupby('county', as_index=False).sum()[['county', 'death']]
death_all_2013 = death_raw_2013.groupby('county', as_index=False).sum()[['county', 'death']]
death_all_2014 = death_raw_2014.groupby('county', as_index=False).sum()[['county', 'death']]
death_all_2015 = death_raw_2015.groupby('county', as_index=False).sum()[['county', 'death']]

# import the county population data
data_pop = pd.read_csv('./Auxiliary/County_population.csv')
population_raw_2012 = data_pop[['fips', 'county_state', '2012_population']]
population_raw_2013 = data_pop[['fips', 'county_state', '2013_population']]
population_raw_2014 = data_pop[['fips', 'county_state', '2014_population']]
population_raw_2015 = data_pop[['fips', 'county_state', '2015_population']]

# split the county_state column 
population_raw_2012['county'] = population_raw_2012['county_state'].str.split(',', expand=True)[0]
population_raw_2013['county'] = population_raw_2013['county_state'].str.split(',', expand=True)[0]
population_raw_2014['county'] = population_raw_2014['county_state'].str.split(',', expand=True)[0]
population_raw_2015['county'] = population_raw_2015['county_state'].str.split(',', expand=True)[0]

# first combine death_all_XXXX and population_raw_XXXX
death_all_merged_2012 = population_raw_2012.merge(death_all_2012, left_on = 'county', right_on = 'county', how = 'left').fillna(0)
death_all_merged_2013 = population_raw_2013.merge(death_all_2013, left_on = 'county', right_on = 'county', how = 'left').fillna(0)
death_all_merged_2014 = population_raw_2014.merge(death_all_2014, left_on = 'county', right_on = 'county', how = 'left').fillna(0)
death_all_merged_2015 = population_raw_2015.merge(death_all_2015, left_on = 'county', right_on = 'county', how = 'left').fillna(0)

# compute (#deaths/#population)*100000
death_all_merged_2012['death_normalization'] = death_all_merged_2012['death']*100000/death_all_merged_2012['2012_population']
death_all_merged_2013['death_normalization'] = death_all_merged_2013['death']*100000/death_all_merged_2013['2013_population']
death_all_merged_2014['death_normalization'] = death_all_merged_2014['death']*100000/death_all_merged_2014['2014_population']
death_all_merged_2015['death_normalization'] = death_all_merged_2015['death']*100000/death_all_merged_2015['2015_population']


# first combine tweet_count_XXXX and population_raw_XXXX
tweet_count_merged_2012 = tweet_count_2012.merge(population_raw_2012, left_on='county', right_on='county')
tweet_count_merged_2013 = tweet_count_2013.merge(population_raw_2013, left_on='county', right_on='county')
tweet_count_merged_2014 = tweet_count_2014.merge(population_raw_2014, left_on='county', right_on='county')
tweet_count_merged_2015 = tweet_count_2015.merge(population_raw_2015, left_on='county', right_on='county')

# compute (#abuse/#population)*100000
tweet_count_merged_2012['abuse'] = tweet_count_merged_2012['label_1'] * 100000 / tweet_count_merged_2012['2012_population']
tweet_count_merged_2013['abuse'] = tweet_count_merged_2013['label_1'] * 100000 / tweet_count_merged_2013['2013_population']
tweet_count_merged_2014['abuse'] = tweet_count_merged_2014['label_1'] * 100000 / tweet_count_merged_2014['2014_population']
tweet_count_merged_2015['abuse'] = tweet_count_merged_2015['label_1'] * 100000 / tweet_count_merged_2015['2015_population']


if flag == 0:
    # generate heatmap for death count
    # first add all the death count together
    values_total = death_all_merged_2012['death_normalization']+death_all_merged_2013['death_normalization']+death_all_merged_2014['death_normalization']+death_all_merged_2015['death_normalization']
    # find optimized parameters for plotting
    colorscale = ['RGB(255,'+ str(i) + ','+str(i) +')' for i in range(255, 100, -10)]
    endpts = list(np.linspace(0, np.mean(values_total)+ np.std(values_total),len(colorscale)-1))
    # transform fips and death_normalization to list form
    fips = death_all_merged_2014['fips'].tolist()
    values = death_all_merged_2014['death_normalization'].tolist()
    fig = ff.create_choropleth(fips=fips, values=values, scope = ['PA'], colorscale = colorscale, binning_endpoints=endpts, county_outline={'color': 'rgb(210,150,150)', 'width': 0.5}, state_outline = {'color': 'rgb(210,150,150)', 'width': 0.5}, showlegend = False, plot_bgcolor = 'RGB(255, 255, 255)')
    #fig.show()
    fig.write_image("./test.png")
else:
    # generate heatmap for abuse count
    # first add all the abuse count together
    values_total = tweet_count_merged_2012['abuse'] + tweet_count_merged_2013['abuse'] + tweet_count_merged_2014['abuse'] + tweet_count_merged_2015['abuse']
    # find optimized parameters for plotting
    colorscale = ['RGB(255,' + str(i) + ',' + str(i) + ')' for i in range(255, 100, -10)]
    endpts = list(np.linspace(0, np.mean(values_total) + np.std(values_total), len(colorscale) - 1))

    # transform fips and abuse to list form
    fips = tweet_count_merged_2014['fips'].tolist()
    values = tweet_count_merged_2014['abuse'].tolist()

    fig = ff.create_choropleth(fips=fips, values=values, scope=['PA'], colorscale=colorscale, binning_endpoints=endpts, county_outline={'color': 'rgb(210,150,150)', 'width': 0.5},
                               state_outline={'color': 'rgb(210,150,150)', 'width': 0.5}, showlegend=False, plot_bgcolor='RGB(255, 255, 255)')
    #fig.show()
    fig.write_image("./test.png")
