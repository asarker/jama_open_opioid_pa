'''
***************************************************************************************************
This script accompanies the following paper:

Sarker A, Gonzalez-Hernandez G, Ruan Y, Perrone J.
Machine Learning and Natural Language Processing for Geolocation-Centric Monitoring and
Characterization of Opioid-Related Social Media Chatter.
JAMA Netw Open. 2019 Nov 1;2(11):e1914672. doi: 10.1001/jamanetworkopen.2019.14672.

The script was modified from several online examples. Please follow the parameter values specified
in the paper to obtain comparable results.

For questions, please contact:
Abeed Sarker (abeed@dbmi.emory.edu)
Yucheng Ruan (ycruan@seas.upenn.edu)

'''

from sklearn.metrics import accuracy_score,precision_recall_fscore_support
from collections import defaultdict
import string
from sklearn.metrics import confusion_matrix


import itertools
import operator

def most_common(L):
  SL = sorted((x, i) for i, x in enumerate(L))
  groups = itertools.groupby(SL, key=operator.itemgetter(0))
  def _auxfun(g):
    item, iterable = g
    count = 0
    min_index = len(L)
    for _, where in iterable:
      count += 1
      min_index = min(min_index, where)
    return count, -min_index
  return max(groups, key=_auxfun)[0]

def getPrediction(pred_arr):
    '''
    given the predictions of 4 individual classifiers, generate a final prediction
    :param pred_arr:
    :return:
    '''
    most_common_preds = defaultdict()
    for k,v in pred_arr.items():
        print k,v,most_common(v)
        if v.count(0)>=2:
            most_common_preds[k] = 0
        elif v.count(2)>=2:
            most_common_preds[k]=2
        else:
            most_common_preds[k]=most_common(v)
    return most_common_preds

preds = defaultdict(list)
oracs = defaultdict(int)

final_preds = []
oracle = []

infile = open('predictions2.txt')
for line in infile:
    items = string.strip(line).split('\t')
    pred = int(items[1])-1
    orac = int(items[2])
    preds[items[0]].append(pred)
    oracs[items[0]]=int(items[2])

infile = open('test_set_predictions_rf.txt')
for line in infile:
    items = string.strip(line).split('\t')
    pred = int(items[1])-1
    preds[items[0]].append(pred)

infile = open('test_set_predictions_svm.txt')
for line in infile:
    items = string.strip(line).split('\t')
    pred = int(items[1])-1
    preds[items[0]].append(pred)

infile = open('test_set_predictions_nb.txt')
for line in infile:
    items = string.strip(line).split('\t')
    pred = int(items[1])-1
    preds[items[0]].append(pred)

'''
infile = open('test_set_predictions_dt.txt')
for line in infile:
    items = string.strip(line).split('\t')
    pred = int(items[1])-1
    preds[items[0]].append(pred)

'''
'''
infile = open('test_set_predictions_nn.txt')
for line in infile:
    items = string.strip(line).split('\t')
    pred = int(items[1])-1
    preds[items[0]].append(pred)
'''
'''
infile = open('test_set_predictions_dt.txt')
for line in infile:
    items = string.strip(line).split('\t')
    pred = int(items[1])-1
    preds[items[0]].append(pred)
'''

fin_pr = getPrediction(preds)

for k in preds.keys():
    final_preds.append(fin_pr[k])
    oracle.append(oracs[k])


print precision_recall_fscore_support(oracle, final_preds)
print precision_recall_fscore_support(oracle, final_preds, average='micro')
print precision_recall_fscore_support(oracle, final_preds, average='macro')
print confusion_matrix(oracle, final_preds)