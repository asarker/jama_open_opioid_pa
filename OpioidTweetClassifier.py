'''
***************************************************************************************************
This script accompanies the following paper:

Sarker A, Gonzalez-Hernandez G, Ruan Y, Perrone J.
Machine Learning and Natural Language Processing for Geolocation-Centric Monitoring and
Characterization of Opioid-Related Social Media Chatter.
JAMA Netw Open. 2019 Nov 1;2(11):e1914672. doi: 10.1001/jamanetworkopen.2019.14672.

The script was modified from several online examples. Please follow the parameter values specified
in the paper to obtain comparable results.

For questions, please contact:
Abeed Sarker (abeed@dbmi.emory.edu)
Yucheng Ruan (ycruan@seas.upenn.edu)

'''

import csv, string, re, bs4
from nltk.stem.porter import *
stemmer = PorterStemmer()
from nltk.corpus import stopwords
from collections import defaultdict
import os
import numpy as np
import nltk
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import svm, model_selection
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import precision_recall_fscore_support,accuracy_score
from sklearn import naive_bayes, tree
from sklearn.metrics import confusion_matrix
from sklearn.neighbors import KNeighborsClassifier
st = stopwords.words('english')
import sys

reload(sys)
sys.setdefaultencoding('utf8')

'''
FUNCTIONS...
'''
def loadAllTrainingData(dir_path):
    '''
    Given the directory path, load all the data

    :param dir_path: the path to the directory
    :return: a dictionary containing all the instances indexed by the ID
    '''
    print 'Loading all annotated data...'
    all_instances = defaultdict()
    class_distrib = []
    count = 0
    dirlist = os.listdir(dir_path)
    for f in dirlist:
        print f
        #load only the training and the dev sets.
        if re.search('train',f) or re.search('dev',f):
            infile = open(dir_path + f)
            for line in infile:
                items = line.split('\t')
                id_ = items[0]
                tweet = string.strip(items[4])
                class_ = string.strip(items[3])
                if len(items) >= 3:
                    class_distrib.append(class_)
                    count += 1
                    all_instances[id_] = (tweet, class_)
    total_instances = len(all_instances.keys())
    class_fd = nltk.FreqDist(class_distrib)
    print 'Total number of training instances:'
    print total_instances
    print 'Distribution of classes in training and development sets:'
    for k, v in class_fd.items():
        print k, '\t', v, '\t', str(round(100.0 * ((v + 0.0) / total_instances), 2)), '%'
    return all_instances

def loadAllTestData(dir_path):
    '''
        Given the directory path, load all the data

        :param dir_path: the path to the directory
        :return: a dictionary containing all the instances indexed by the ID
        '''
    print 'Loading all annotated data...'
    all_instances = defaultdict()
    class_distrib = []
    count = 0
    dirlist = os.listdir(dir_path)
    for f in dirlist:
        print f
        # load only the training and the dev sets.
        if re.search('test', f):
            infile = open(dir_path + f)
            for line in infile:
                items = line.split('\t')
                id_ = items[0]
                tweet = string.strip(items[4])
                class_ = string.strip(items[3])
                if len(items) >= 3:
                    class_distrib.append(class_)
                    count += 1
                    all_instances[id_] = (tweet, class_)
    total_instances = len(all_instances.keys())
    class_fd = nltk.FreqDist(class_distrib)
    print 'Total number of test instances:'
    print total_instances
    print 'Distribution of classes in test set:'
    for k, v in class_fd.items():
        print k, '\t', v, '\t', str(round(100.0 * ((v + 0.0) / total_instances), 2)), '%'
    return all_instances


'''
def loadDNNAnnotatedData(dir_path):
    print 'Loading DNN annotated data ... '
    all_instances = defaultdict()
    class_distrib = []
    dirlist = os.listdir(dir_path)
'''

def removeSpecificPOS(spacy_object, listoftags):
    new_string = ''
    for token in spacy_object:
        if not token.pos_ in listoftags:
            new_string += ' ' + token.text

        else:
            print token.text, '\t', token.pos_

    return new_string


def loadClustersPerTweet(filepath):
    word_clusters = {}
    infile = open(filepath)
    for line in infile:
        items = line.split('\t')
        word_clusters[items[0]] = string.strip(items[1])
    return word_clusters


def getstructuralfeatures(processed_tweet):
    tweet_len_char = len(processed_tweet)
    tweet_len_word = len(processed_tweet.split())
    return tweet_len_char, tweet_len_word


def loadChildrenNames(filepath):
    names = []
    infile = open(filepath)
    for line in infile:
        names.append(string.strip(string.lower(line)))
    return names


def preprocess_text(tweet_text):
    '''
    :param tweet_text: the tweet to preprocess
    :return: a string containing the preprocessed text
    '''
    # Replace "&amp;"
    tweet_text = re.sub(r'&amp;', "and", tweet_text)

    # Replace/remove URL
    tweet_text = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '_url_',
                        tweet_text)

    # Replace/remove username
    tweet_text = re.sub('(@[A-Za-z0-9\_]+)', '_username_', tweet_text)
    # tweet_text = re.sub('(@[A-Za-z0-9\_]+)', " ", tweet_text)

    # Remove non-letters
    tweet_text = re.sub("[^a-zA-Z_]", " ", tweet_text)

    # Remove hashtags
    # tweet_text = re.sub("#", " ", tweet_text)

    # Lowercase
    tweet_text = tweet_text.lower()

    # Remove extra whitespace (spaCy mistags contiguous whitespace as entities)
    tweet_text = re.sub(r'\s{2,}', " ", tweet_text)

    # Stem
    tweet_text = ' '.join([stemmer.stem(w) for w in tweet_text.split()])

    return tweet_text


def loadWordClusters():
    '''

    :return: word clusters..
    '''
    word_clusters = defaultdict()
    #File available at: https://github.com/brendano/ark-tweet-nlp/blob/master/ark-tweet-nlp/src/main/resources/cmu/arktweetnlp/50mpaths2
    infile = open('50mpaths2.txt')
    for line in infile:
        items = line.split()
        class_ = items[0]
        term = items[1]
        word_clusters[term] = class_
    return word_clusters


def loadAbuseIndicatingTerms(fpath):
    abuse_terms = []
    infile = open(fpath)
    for line in infile:
        abuse_terms.append(string.strip(line))
    return abuse_terms


def getTextsAndClasses(data_dict):
    '''
    Loads the ids, texts and classes for instances from a dictionary format
    :param data_dict:
    :return:
    '''
    ids = []
    texts = []
    classes = []

    for k, v in data_dict.items():
        ids.append(k)
        texts.append(v[0])
        classes.append(v[1])

    return ids, texts, classes


def get_wordcluster_string(text, wc):
    wc_string = ''
    tokens = text.split()
    for t in tokens:
        try:
            wc_string += ' _c' + wc[t] + 'c_ '
        except KeyError:
            pass
    # print wc_string
    return wc_string


def get_abuse_terms_vector(text, abuse_terms):
    ats = []
    for at in abuse_terms:
        if re.search(at, text):
            ats.append(1)
        else:
            ats.append(0)
    return ats

if __name__ == '__main__':
    # LOADER
    training_annotated_instances = loadAllTrainingData('./LabeledData/')
    test_data_annotated_instances = loadAllTestData('./LabeledData/')
    word_clusters = loadWordClusters()
    abuse_terms = loadAbuseIndicatingTerms('./Auxiliary/abuse_indicating_terms.tsv')

    training_abuse_terms_vector = []
    test_abuse_terms_vector = []

    training_data_ids, training_data_texts, training_data_classes = getTextsAndClasses(training_annotated_instances)
    test_data_ids, test_data_texts, test_data_classes = getTextsAndClasses(test_data_annotated_instances)

    preprocessed_training_data_texts = []
    preprocessed_test_data_texts = []
    training_word_cluster_strings = []
    test_word_cluster_strings = []
    for ttext in training_data_texts:
        preprocessed_training_data_texts.append(preprocess_text(ttext))
        training_word_cluster_strings.append(get_wordcluster_string(ttext, word_clusters))
        training_abuse_terms_vector.append(get_abuse_terms_vector(ttext, abuse_terms))

    for ttext in test_data_texts:
        preprocessed_test_data_texts.append(preprocess_text(ttext))
        test_word_cluster_strings.append(get_wordcluster_string(ttext,word_clusters))
        test_abuse_terms_vector.append(get_abuse_terms_vector(ttext,abuse_terms))


    # Define the classifier
    vectorizer = CountVectorizer(ngram_range=(1, 3), analyzer="word", tokenizer=None, preprocessor=None,
                                 max_features=5000)
    wc_vectorizer = CountVectorizer(ngram_range=(1, 1), analyzer="word", tokenizer=None, preprocessor=None,
                                    max_features=2000)

    trained_data_vectors = vectorizer.fit_transform(preprocessed_training_data_texts).toarray()
    trained_data_cluster_vectors = wc_vectorizer.fit_transform(training_word_cluster_strings).toarray()

    combined_training_vectors = np.concatenate((trained_data_vectors, trained_data_cluster_vectors), axis=1)
    combined_training_vectors = np.concatenate((combined_training_vectors, training_abuse_terms_vector), axis=1)

    test_data_vectors = vectorizer.transform(preprocessed_test_data_texts).toarray()
    test_data_cluster_vectors = wc_vectorizer.transform(test_word_cluster_strings).toarray()

    combined_test_vectors = np.concatenate((test_data_vectors, test_data_cluster_vectors), axis=1)
    combined_test_vectors = np.concatenate((combined_test_vectors, test_abuse_terms_vector),axis=1)



    print 'Running Classification Training... This May Take A While'

    c = 140
    n_estimators = 10000
    k = 10
    #UNCOMMEND WHEN USING SVM CLASSIFIER
    svm_classifier = svm.SVC(C=c, cache_size=200, coef0=0.0, degree=3, gamma='auto', kernel='rbf', max_iter=-1,
                             probability=False, random_state=None, shrinking=True, tol=0.001, verbose=False)

    prediction_svm_classifier = svm_classifier.fit(combined_training_vectors,training_data_classes)
    predictions = prediction_svm_classifier.predict(combined_test_vectors)

    #UNCOMMENT WHEN USING RANDOM FOREST CLASSIFIER
    #rf = RandomForestClassifier(n_estimators=(10000), oob_score=True, random_state=123456)
    #rf = rf.fit(combined_training_vectors,training_data_classes)
    #predictions = rf.predict(combined_test_vectors)

    #UNCOMMENT WHEN USING DECISION TREE CLASSIFIER
    #clf = tree.DecisionTreeClassifier()
    #clf = clf.fit(combined_training_vectors,training_data_classes)
    #predictions = clf.predict(combined_test_vectors)

    #UNCOMMENT WHEN USING KNN CLASSIFIER
    #clf = KNeighborsClassifier(n_neighbors=10)
    #clf.fit(combined_training_vectors,training_data_classes)
    #predictions = clf.predict(combined_test_vectors)

    #UNCOMMENT WHEN USING NAIVE BAYES CLASSIFIER
    #nb = naive_bayes.GaussianNB()
    #nb = nb.fit(combined_training_vectors,training_data_classes)
    #predictions = nb.predict(combined_test_vectors)

    #EVALUATE AND DISPLAY RESULTS, INCLUDING CONFUSION MATRIX
    print precision_recall_fscore_support(test_data_classes, predictions)
    print precision_recall_fscore_support(test_data_classes,predictions,average='macro')
    print precision_recall_fscore_support(test_data_classes,predictions,average='micro')
    print accuracy_score(test_data_classes,predictions)
    print confusion_matrix(test_data_classes, predictions)
