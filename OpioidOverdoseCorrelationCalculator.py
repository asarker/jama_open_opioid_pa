'''
***************************************************************************************************
This script accompanies the following paper:

Sarker A, Gonzalez-Hernandez G, Ruan Y, Perrone J.
Machine Learning and Natural Language Processing for Geolocation-Centric Monitoring and
Characterization of Opioid-Related Social Media Chatter.
JAMA Netw Open. 2019 Nov 1;2(11):e1914672. doi: 10.1001/jamanetworkopen.2019.14672.

The script was modified from several online examples. Please follow the parameter values specified
in the paper to obtain comparable results.

For questions, please contact:
Abeed Sarker (abeed@dbmi.emory.edu)
Yucheng Ruan (ycruan@seas.upenn.edu)

'''

from scipy.stats import pearsonr,spearmanr
import string, nltk, csv
import pandas as pd
from collections import defaultdict
import numpy as np
deathrate = []
tweetrate = []

for year in ['2012','2013','2014']:
    infile = open('./CorrelationData/DeathRate'+year+'.txt')
    deaths = defaultdict(int)
    population = defaultdict(int)
    for line in infile:

        items = string.strip(line).split('\t')
        if (not items[-1]=='Unreliable') or (items[-1]=='Unreliable' and deaths.has_key(items[0])):
            county=items[0].split(',')[0]
            deaths[county]+=int(items[-3])
            population[county]=int(items[-2])
    crudedeathrates = defaultdict(float)
    for k,v in deaths.items():

        crudedeathrates[k]=(float(v)/population[k])*100000

    totaltweets = defaultdict(int)
    abusetweets = defaultdict(int)
    informationtweets = defaultdict(int)
    unrelatedtweets = defaultdict(int)

    infile = open('./CorrelationData/TweetCountUnf'+year+'.txt')
    for line in infile:
        items = line.split('\t')
        totaltweets[items[0]]=int(items[1])
        abusetweets[items[0]]=int(items[2])
        informationtweets[items[0]]=int(items[4])
        unrelatedtweets[items[0]]=int(items[3])


    for k,v in crudedeathrates.items():
        if int(deaths[k])>10:
            deathrate.append(v)

            if float(abusetweets[k])==0:
                tweetrate.append(0)
            else:
                tweetrate.append(float(abusetweets[k]*100000)/population[k])#
print len(deathrate),len(tweetrate)
print pearsonr(deathrate,tweetrate)
print spearmanr(deathrate,tweetrate)
