'''
***************************************************************************************************
This script accompanies the following paper:

Sarker A, Gonzalez-Hernandez G, Ruan Y, Perrone J.
Machine Learning and Natural Language Processing for Geolocation-Centric Monitoring and
Characterization of Opioid-Related Social Media Chatter.
JAMA Netw Open. 2019 Nov 1;2(11):e1914672. doi: 10.1001/jamanetworkopen.2019.14672.

The script was modified from several online examples. Please follow the parameter values specified
in the paper to obtain comparable results.

For questions, please contact:
Abeed Sarker (abeed@dbmi.emory.edu)
Yucheng Ruan (ycruan@seas.upenn.edu)

'''

import pandas as pd

# library for geographic plotting
import plotly.figure_factory as ff
from sklearn.preprocessing import MinMaxScaler

# read the geocodes into data frame
geocodes = pd.read_csv('./Auxiliary/County_Pennsylvania.txt', delimiter='\t', header=None, encoding='latin1', dtype=str)
geocodes.columns = ['summary_level', 'states_code', 'county_code', 'county_sub_code', 'place_code', 'consol_city_code', 'area_name']

# add fips column
geocodes['fips'] = geocodes['states_code'] + geocodes['county_code']
# extract the area_name and fips columns
geocodes_filtered = geocodes[['area_name', 'fips']]

# load the substate data
raw_substate = pd.read_csv('../data/data_heatmap/pa_substate_regions_and_abuse_tweets.csv', dtype=object)

# initialize a dataframe
data_combined_df = pd.concat([geocodes_filtered, pd.DataFrame(
    columns=['Illicit drug use, no marijuana, past month', 'nonmedical use of pain relievers, past year', 'illicit drug dependence or abuse, past year', 'illicit drug dependence, past year',
             'tweets'])], sort=False)
# fillna
data_combined_df = data_combined_df.fillna(0)

data_combined_df['area_name'] = data_combined_df['area_name'].apply(str.lower)

# iterate the rows in raw_substate
for index, row in raw_substate.iterrows():
    # split the counties into list
    temp_counties = row['Counties'].split()

    # ierate every ele in list
    for county in temp_counties:
        data_combined_df.loc[data_combined_df['area_name'] == (county + ' county'), ['Illicit drug use, no marijuana, past month', 'nonmedical use of pain relievers, past year',
                                                                                     'illicit drug dependence or abuse, past year', 'illicit drug dependence, past year', 'tweets']] = row[
            ['Illicit drug use, no marijuana, past month', 'nonmedical use of pain relievers, past year', 'illicit drug dependence or abuse, past year', 'illicit drug dependence, past year',
             'Tweet numbers']].values


print(data_combined_df.count())

# scale all the 5 columns
scaler = MinMaxScaler()
data_combined_df.iloc[:, 2:] = scaler.fit_transform(data_combined_df.iloc[:, 2:])

# transform fips and death_normalization to list form
fips = data_combined_df['fips'].tolist()
values_1 = data_combined_df['Illicit drug use, no marijuana, past month'].astype(float).tolist()
colorscale_1 = ['RGB(255,' + str(i) + ',' + str(i) + ')' for i in [250, 245, 240, 230, 220, 210, 200, 180, 160, 150, 110, 100, 60, 50]]
endpts_1 = sorted(set(values_1))

fips = data_combined_df['fips'].tolist()
values_2 = data_combined_df['nonmedical use of pain relievers, past year'].astype(float).tolist()
colorscale_2 = ['RGB(255,' + str(i) + ',' + str(i) + ')' for i in [250, 245, 240, 230, 220, 210, 200, 180, 160, 150, 110, 100, 80, 70]]
endpts_2 = sorted(set(values_2))

values_3 = data_combined_df['illicit drug dependence or abuse, past year'].astype(float).tolist()
colorscale_3 = ['RGB(255,' + str(i) + ',' + str(i) + ')' for i in [250, 245, 240, 230, 220, 210, 200, 180, 160, 150, 110, 100, 60, 50]]
endpts_3 = sorted(set(values_3))

values_4 = data_combined_df['illicit drug dependence, past year'].astype(float).tolist()
colorscale_4 = ['RGB(255,' + str(i) + ',' + str(i) + ')' for i in [250, 245, 240, 230, 220, 210, 200, 180, 160, 150, 110, 100, 60, 50]]
endpts_4 = sorted(set(values_4))

values_5 = data_combined_df['tweets'].astype(float).tolist()
colorscale_5 = ['RGB(255,' + str(i) + ',' + str(i) + ')' for i in [250, 245, 240, 230, 220, 210, 200, 190, 180, 170, 160, 130, 90, 40]]
endpts_5 = sorted(set(values_5))

fig_1 = ff.create_choropleth(fips=fips, values=values_1, scope=['PA'], colorscale=colorscale_1, binning_endpoints=endpts_1, county_outline={'color': 'rgb(210,150,150)', 'width': 0.5},
                             state_outline={'color': 'rgb(210,150,150)', 'width': 0.5}, showlegend=False, plot_bgcolor='RGB(255, 255, 255)', width=1800, height=900)
#fig_1.show()
fig_1.write_image("./Illicit drug use, no marijuana, past month.eps")


fig_2 = ff.create_choropleth(fips=fips, values=values_2, scope=['PA'], colorscale=colorscale_2, binning_endpoints=endpts_2, county_outline={'color': 'rgb(210,150,150)', 'width': 0.5},
                             state_outline={'color': 'rgb(210,150,150)', 'width': 0.5}, showlegend=False, plot_bgcolor='RGB(255, 255, 255)', width=1800, height=900)
#fig_2.show()
fig_2.write_image("./nonmedical use of pain relievers, past year.eps")

fig_3 = ff.create_choropleth(fips=fips, values=values_3, scope=['PA'], colorscale=colorscale_3, binning_endpoints=endpts_3, county_outline={'color': 'rgb(210,150,150)', 'width': 0.5},
                             state_outline={'color': 'rgb(210,150,150)', 'width': 0.5}, showlegend=False, plot_bgcolor='RGB(255, 255, 255)', width=1800, height=900)
#fig_3.show()
fig_3.write_image("./illicit drug dependence or abuse, past year.eps")

fig_4 = ff.create_choropleth(fips=fips, values=values_4, scope=['PA'], colorscale=colorscale_4, binning_endpoints=endpts_4, county_outline={'color': 'rgb(210,150,150)', 'width': 0.5},
                             state_outline={'color': 'rgb(210,150,150)', 'width': 0.5}, showlegend=False, plot_bgcolor='RGB(255, 255, 255)', width=1800, height=900)
#fig_4.show()
fig_4.write_image("./illicit drug dependence, past year.eps")

fig_5 = ff.create_choropleth(fips=fips, values=values_5, scope=['PA'], colorscale=colorscale_5, binning_endpoints=endpts_5, county_outline={'color': 'rgb(210,150,150)', 'width': 0.5},
                             state_outline={'color': 'rgb(210,150,150)', 'width': 0.5}, showlegend=False, plot_bgcolor='RGB(255, 255, 255)', width=1800, height=900)
#fig_5.show()
fig_5.write_image("./tweets.eps")